/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/server/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/server/controller/Forwarder.js":
/*!********************************************!*\
  !*** ./src/server/controller/Forwarder.js ***!
  \********************************************/
/*! exports provided: httpToHttps */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "httpToHttps", function() { return httpToHttps; });
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
// @ts-check

/**
 *
 * @param {Request} req
 * @param {Response} res
 */

function httpToHttps(req, res) {
  console.log(req);
  const url = req.path ? req.path : "/";
  const baseDomain = req.headers.host;
  res.writeHead(302, {
    Location: `https://${baseDomain}${url}`
  });
  res.end();
}

/***/ }),

/***/ "./src/server/controller/Photos.js":
/*!*****************************************!*\
  !*** ./src/server/controller/Photos.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _model_Page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../model/Page */ "./src/server/model/Page.js");
//@ts-check


/**
 *
 * @param {Request} req
 * @param {Response} res
 */

/* harmony default export */ __webpack_exports__["default"] = (async function (req, res) {
  const photos = new Array(10).fill(0).map((cur, index) => "/assets/img/gallery_" + (index + 1) + ".jpg");
  res.render("photos", { ...(await new _model_Page__WEBPACK_IMPORTED_MODULE_1__["default"]("services").getPageData()),
    photos
  });
});

/***/ }),

/***/ "./src/server/env.js":
/*!***************************!*\
  !*** ./src/server/env.js ***!
  \***************************/
/*! exports provided: ENABLE_DEBUG */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ENABLE_DEBUG", function() { return ENABLE_DEBUG; });
const ENABLE_DEBUG = false;

/***/ }),

/***/ "./src/server/index.js":
/*!*****************************!*\
  !*** ./src/server/index.js ***!
  \*****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! http */ "http");
/* harmony import */ var http__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(http__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var https__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! https */ "https");
/* harmony import */ var https__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(https__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _controller_Forwarder__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./controller/Forwarder */ "./src/server/controller/Forwarder.js");
/* harmony import */ var _routes_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./routes/router */ "./src/server/routes/router.js");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _env__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./env */ "./src/server/env.js");
// @ts-check








if (_env__WEBPACK_IMPORTED_MODULE_6__["ENABLE_DEBUG"]) {
  const app = express__WEBPACK_IMPORTED_MODULE_0___default()();
  app.set("view engine", "pug");
  app.set("views", "../src/server/view");
  app.use(express__WEBPACK_IMPORTED_MODULE_0___default.a.static("public"));
  app.use(_routes_router__WEBPACK_IMPORTED_MODULE_4__["default"]);
  const server = http__WEBPACK_IMPORTED_MODULE_1___default.a.createServer(app).listen(80, "127.0.0.1");
} else {
  const options = {
    key: fs__WEBPACK_IMPORTED_MODULE_5___default.a.readFileSync("./ssl/key.pem", {
      encoding: "UTF-8"
    }),
    cert: fs__WEBPACK_IMPORTED_MODULE_5___default.a.readFileSync("./ssl/cert.pem", {
      encoding: "UTF-8"
    })
  };
  const app = express__WEBPACK_IMPORTED_MODULE_0___default()();
  app.set("view engine", "pug");
  app.set("views", "../src/server/view");
  app.use(express__WEBPACK_IMPORTED_MODULE_0___default.a.static("public"));
  app.use(_routes_router__WEBPACK_IMPORTED_MODULE_4__["default"]);
  const redirect = http__WEBPACK_IMPORTED_MODULE_1___default.a.createServer(_controller_Forwarder__WEBPACK_IMPORTED_MODULE_3__["httpToHttps"]).listen(80);
  const server = https__WEBPACK_IMPORTED_MODULE_2___default.a.createServer(options, app).listen(443);
}

/***/ }),

/***/ "./src/server/model/Page.js":
/*!**********************************!*\
  !*** ./src/server/model/Page.js ***!
  \**********************************/
/*! exports provided: Page, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Page", function() { return Page; });
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ "fs");
/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);

class Page {
  constructor(name) {
    this.name = name;
  }

  getMetaJSON() {
    return new Promise((res, rej) => {
      fs__WEBPACK_IMPORTED_MODULE_0___default.a.readFile(`${process.env.PWD}/json/meta.json`, {
        encoding: "UTF-8"
      }, (err, data) => {
        if (err) rej(err);
        res(data);
      });
    });
  }

  async getTitle() {
    const json = JSON.parse((await this.getMetaJSON()));
    Object.keys(json).forEach(key => {
      if (key === this.name) return json[key].title;
    });
    return "This page has no title";
  }

  async getDescription() {
    const json = JSON.parse((await this.getMetaJSON()));
    Object.keys(json).forEach(key => {
      if (key === this.name) return json[key].description;
    });
    return "This page has no description.";
  }

  async getPageData() {
    return {
      title: await this.getTitle(),
      description: await this.getDescription()
    };
  }

}
/* harmony default export */ __webpack_exports__["default"] = (Page);

/***/ }),

/***/ "./src/server/routes/router.js":
/*!*************************************!*\
  !*** ./src/server/routes/router.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! express */ "express");
/* harmony import */ var express__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(express__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _controller_Photos__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../controller/Photos */ "./src/server/controller/Photos.js");
/* harmony import */ var _model_Page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/Page */ "./src/server/model/Page.js");



const route = Object(express__WEBPACK_IMPORTED_MODULE_0__["Router"])();
route.get(["/home", "/"], async (req, res) => res.render("home", (await new _model_Page__WEBPACK_IMPORTED_MODULE_2__["default"]("home").getPageData())));
route.get("/services", async (req, res) => res.render("services", (await new _model_Page__WEBPACK_IMPORTED_MODULE_2__["default"]("services").getPageData())));
route.get("/photos", _controller_Photos__WEBPACK_IMPORTED_MODULE_1__["default"]);
/* harmony default export */ __webpack_exports__["default"] = (route);

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),

/***/ "http":
/*!***********************!*\
  !*** external "http" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("http");

/***/ }),

/***/ "https":
/*!************************!*\
  !*** external "https" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("https");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map