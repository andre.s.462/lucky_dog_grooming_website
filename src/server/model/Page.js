import fs from "fs";

export class Page {
  constructor(name) {
    this.name = name;
  }
  getMetaJSON() {
    return new Promise((res, rej) => {
      fs.readFile(
        `${process.env.PWD}/json/meta.json`,
        { encoding: "UTF-8" },
        (err, data) => {
          if (err) rej(err);
          res(data);
        }
      );
    });
  }
  async getTitle() {
    const json = JSON.parse(await this.getMetaJSON());
    Object.keys(json).forEach(key => {
      if (key === this.name) return json[key].title;
    });
    return "This page has no title";
  }
  async getDescription() {
    const json = JSON.parse(await this.getMetaJSON());
    Object.keys(json).forEach(key => {
      if (key === this.name) return json[key].description;
    });
    return "This page has no description.";
  }
  async getPageData() {
    return {
      title: await this.getTitle(),
      description: await this.getDescription()
    };
  }
}

export default Page;
