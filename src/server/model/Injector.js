// @ts-check

import fs from "fs";

/**
 *
 * @param {string} filePath
 * @returns {Promise<ReadyToInject>}
 */
export function Injector(filePath) {
  return new Promise((res, rej) => {
    fs.readFile(filePath, { encoding: "UTF-8" }, (err, data) => {
      if (err) {
        console.error(err);
        res(new ReadyToInject(""));
      }
      res(new ReadyToInject(data));
    });
  });
}

export default Injector;

export class ReadyToInject {
  /**
   *
   * @param {string} value
   */
  constructor(value) {
    this.value = value;
  }
  /**
   * @param {string} filePath
   * @param {string | RegExp} lookup
   * @returns {Promise<this>}
   */
  inject(lookup, filePath) {
    return new Promise((res, rej) => {
      fs.readFile(filePath, { encoding: "UTF-8" }, (err, data) => {
        if (err) res(this);
        this.value = this.value.replace(lookup, data);
        res(this);
      });
    });
  }
}
