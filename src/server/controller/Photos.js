//@ts-check
import { Request, Response } from "express";
import Page from "../model/Page";

/**
 *
 * @param {Request} req
 * @param {Response} res
 */
export default async function(req, res) {
  const photos = new Array(10)
    .fill(0)
    .map((cur, index) => "/assets/img/gallery_" + (index + 1) + ".jpg");
  res.render("photos",{ ...await new Page("services").getPageData(), photos});
}
