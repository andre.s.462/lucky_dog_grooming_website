// @ts-check

import { Request, Response } from "express";
/**
 *
 * @param {Request} req
 * @param {Response} res
 */
export function httpToHttps(req, res) {
  console.log(req);
  const url = req.path ? req.path : "/";
  const baseDomain = req.headers.host;
  res.writeHead(302, { Location: `https://${baseDomain}${url}` });
  res.end();
}
