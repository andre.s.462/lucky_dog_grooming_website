// @ts-check

import express from "express";
import http from "http";
import https from "https";
import { httpToHttps } from "./controller/Forwarder";
import routes from "./routes/router";
import fs from "fs";
import * as ENV_VARIABLES from "./env";

if (ENV_VARIABLES.ENABLE_DEBUG) {
  const app = express();
  app.set("view engine", "pug");
  app.set("views", "../src/server/view");
  app.use(express.static("public"));
  app.use(routes);
  const server = http.createServer(app).listen(80, "127.0.0.1");
} else {
  const options = {
    key: fs.readFileSync("./ssl/key.pem", { encoding: "UTF-8" }),
    cert: fs.readFileSync("./ssl/cert.pem", { encoding: "UTF-8" })
  };
  const app = express();
  app.set("view engine", "pug");
  app.set("views", "../src/server/view");
  app.use(express.static("public"));
  app.use(routes);
  const redirect = http.createServer(httpToHttps).listen(80);
  const server = https.createServer(options, app).listen(443);
}
