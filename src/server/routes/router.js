import { Router } from "express";
import Photos from "../controller/Photos";
import Page from "../model/Page";

const route = Router();

route.get(["/home", "/"], async (req, res) =>
  res.render("home", await new Page("home").getPageData())
);

route.get("/services", async (req, res) =>
  res.render("services", await new Page("services").getPageData())
);

route.get("/photos", Photos);

export default route;
